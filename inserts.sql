drop database ameapp;
create database ameapp;

use ameapp;

SELECT * FROM users;

INSERT INTO `ameapp`.`users` (`name`, `email`, `password`, `created_at`, `updated_at`) VALUES ('Aldair Ruiz',
 'aruiz@gmail.com', '123456', now(), now());

SELECT * FROM estilo_aprendizajes;

INSERT INTO estilo_aprendizajes VALUES(DEFAULT, "Activo", now(), now());
INSERT INTO estilo_aprendizajes VALUES(DEFAULT, "Reflexivo", now(), now());
INSERT INTO estilo_aprendizajes VALUES(DEFAULT, "Teórico", now(), now());
INSERT INTO estilo_aprendizajes VALUES(DEFAULT, "Pragmático", now(), now());

INSERT INTO grados VALUES (DEFAULT, 'Primero Preparatoria', now(), now());
SELECT * FROM grados;

INSERT INTO materias VALUES (DEFAULT, 'Programación', 1, now(), now());
INSERT INTO materias VALUES (DEFAULT, 'Matemáticas', 1, now(), now());
INSERT INTO materias VALUES (DEFAULT, 'Español', 1, now(), now());
SELECT * FROM materias ORDER BY id ASC;

7
SELECT * FROM usuario_estilos WHERE usuario_id = 1 order by estilo_id asc;

INSERT INTO `ameapp`.`contenidos` (`nombre`, `descripcion`, `rutaArchivo`, `formatoArchivo`, `dificultad`, `materia_id`, `estilo_id`) VALUES ('Actividad de programación', 'El contenido a mostrar es una actividad instruida de como realizar una clase', 'http://', 'pdf','0', '1', '1');
INSERT INTO `ameapp`.`contenidos` (`nombre`, `descripcion`, `rutaArchivo`,  `formatoArchivo`,`dificultad`,  `materia_id`, `estilo_id`) VALUES ('Ejemplo', 'El contenido es un audio con un ejemplo de como realizar una clase', 'http://',  'pdf','0',  '1', '2');
INSERT INTO `ameapp`.`contenidos` (`nombre`, `descripcion`, `rutaArchivo`,  `formatoArchivo`,`dificultad`,  `materia_id`, `estilo_id`) VALUES ('PDF Programación básica', 'El contenido es una lectura acerca de los conceptos básicos de programación', 'http://',  'pdf', '0',  '1', '3');
INSERT INTO `ameapp`.`contenidos` (`nombre`, `descripcion`, `rutaArchivo`,  `formatoArchivo`,`dificultad`,  `materia_id`, `estilo_id`) VALUES ('Video de conceptos básicos de programación', 'El contenido es un video con práctica guiada de como realizar una clase', 'pdf', 'http://', '0',  '1', '4');

SELECT * FROM contenidos;

SELECT * 
FROM contenidos WHERE estilo_id IN (SELECT estilo_id FROM usuario_estilos 
											 WHERE usuario_id = 1 AND (prioridad LIKE 'MUY ALTA' OR prioridad LIKE 'ALTA' OR prioridad LIKE 'MODERADA')
                                             order by estilo_id asc);

