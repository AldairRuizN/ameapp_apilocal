<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return response()->json($users);
    }

    public function validarUsuario(Request $request)
    {

        $emailRequest =  $request['email'];
        $passRequest =  $request['password'];
        $users = User::all();

        foreach ($users as $user) {
            if(($user['email'] == $emailRequest) && ($user['password'] == $passRequest) ){
                return response()->json([
                    'id' => $user['id'],
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'password' => "",
                    'status' => true,
                    'mensaje' => 'Usuario existe'
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'mensaje' => 'Usuario no existe'
                ]);
            }
        }
    }

    public function show($id)
    {
        return User::findOrFail($id)->get();
    }
}
