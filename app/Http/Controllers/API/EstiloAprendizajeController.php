<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\EstiloAprendizaje;
use Illuminate\Http\Request;

class EstiloAprendizajeController extends Controller
{
    public function index()
    {
        $estilos = EstiloAprendizaje ::all();
        return response()->json($estilos);
    }

    public function store(Request  $request)
    {
        print($request['nombre']);
    }
}
