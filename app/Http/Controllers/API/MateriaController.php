<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Materia;


class MateriaController extends Controller
{
    public function index()
    {
        $materias = Materia ::all();
        return response()->json($materias);
    }
}
