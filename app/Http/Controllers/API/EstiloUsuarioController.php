<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\UsuarioEstilo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;

class EstiloUsuarioController extends Controller
{
    public function index()
    {
        $estilos = UsuarioEstilo ::all();
        return response()->json($estilos);
    }

    public function create()
    {
        //
    }


    public function store(Request  $request)
    {
        $userStyle = new UsuarioEstilo();
        $now = new DateTime();

        $userStyle -> prioridad = $request->input('prioridad');
        $userStyle -> usuario_id = $request->input('usuario_id');
        $userStyle -> estilo_id = $request->input('estilo_id');
        $userStyle -> created_at = $now;
        $userStyle -> updated_at = $now;
        $userStyle -> save();
        return response()->json($userStyle);
    }


    public function show($id)
    {
        $user = DB::table('usuario_estilos')->where('usuario_id', $id)->get();
        return response()->json($user);
    }

    public function userStyleCount($id)
    {
        $user = DB::table('usuario_estilos')->where('usuario_id', $id)->count();
        return response()->json([
            // 'status' => false,
            'count' => $user
        ]);
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
