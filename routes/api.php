<?php

use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\EstiloAprendizajeController;
use App\Http\Controllers\API\EstiloUsuarioController;
use App\Http\Controllers\API\MateriaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::apiResource('users',UserController::class);
Route::apiResource('estilos',EstiloAprendizajeController::class);
Route::apiResource('materias',MateriaController::class);
Route::apiResource('estilosUsuario',EstiloUsuarioController::class);
Route::get('estilosUsuario/count/{id}',[EstiloUsuarioController::class, 'userStyleCount']);
Route::post('estilos',[EstiloAprendizajeController::class, 'store']);
Route::post('users/validar',[UserController::class, 'validarUsuario']);