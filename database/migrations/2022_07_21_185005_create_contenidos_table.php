<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contenidos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->text('descripcion');
            $table->string('rutaArchivo');
            $table->string('formatoArchivo');
            $table->string('dificultad');
            $table->unsignedBigInteger('materia_id');
            $table->unsignedBigInteger('estilo_id');
            $table->foreign('materia_id')->references('id')->on('materias');
            $table->foreign('estilo_id')->references('id')->on('estilo_aprendizajes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contenidos');
    }
};
