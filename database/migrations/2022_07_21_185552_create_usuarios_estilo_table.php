<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_estilos', function (Blueprint $table) {
            $table->id();
            $table->string('prioridad');
            $table->unsignedBigInteger('usuario_id');
            $table->unsignedBigInteger('estilo_id');

            $table->foreign('usuario_id')->references('id')->on('users');
            $table->foreign('estilo_id')->references('id')->on('estilo_aprendizajes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_estilos');
    }
};
