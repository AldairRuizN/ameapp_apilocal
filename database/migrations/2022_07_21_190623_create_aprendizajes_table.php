<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aprendizajes', function (Blueprint $table) {
            $table->id();
            $table->integer('calificacion');
            $table->unsignedBigInteger('usuario_id');
            $table->unsignedBigInteger('contenido_id');

            $table->foreign('usuario_id')->references('id')->on('users');
            $table->foreign('contenido_id')->references('id')->on('contenidos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aprendizajes');
    }
};
